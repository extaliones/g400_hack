// Simple hack to change dpi on Logitech G400
// G400 only support: 400, 800, 1800 and 3600 dpi, all other are interpolate by software
// compile with: gcc -o g400_hack h400_hack.c -lusb-1.0 
//
// author: Przemek Aksamit <extaliones dot gmail.com>

#include <stdio.h>
#include <stdlib.h>
#include <libusb-1.0/libusb.h>

#define G400_VENDOR_ID  0x046d
#define G400_PRODUCT_ID 0xc245

libusb_context *ctx = NULL;
libusb_device_handle *handle = NULL;

int detached = 0;
int claimed  = 0;

void clean_exit(char* error);

int main(int argc, char **argv)
{
  if (argc != 2 || argv[1][0] != '-') {
    printf("Avaliable options: -400 -800 -1800 -3600\n");
    return 1;
  }

  char x = argv[1][1];

  // 0 => 400dpi, 1 => 800dpi, 2 => 1800dpi, 3 => 3600dpi
  int dpi_idx = x == '4' ? 0 : x == '8' ? 1 : x == '1' ? 2 : 3;

  libusb_device_handle *handle = NULL;
  libusb_context *ctx = NULL;

  libusb_init(&ctx);

  handle = libusb_open_device_with_vid_pid(ctx, G400_VENDOR_ID, G400_PRODUCT_ID);

  if (!handle)
    clean_exit("Logitech G400 not found! (you have usb rights? try with sudo?)\n");

  if (libusb_kernel_driver_active(handle, 1) == 1) {
    if (libusb_detach_kernel_driver(handle, 1) != 0)
      clean_exit("Can't detach kernel driver.\n"); 

    detached = 1;
  }

  if (libusb_claim_interface(handle, 1) != 0)
    clean_exit("Failed to claim interface.");

  claimed = 1;

  char data[] = {0x8e, 0x03 + dpi_idx}; 

  //0x21 => USB_ENDPOINT_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE
  if (libusb_control_transfer (handle, 0x21, 9, 0x038e, 1, &data[0], 2, 1000) != 0) 
    clean_exit("Error writing to USB device");

  clean_exit("Finished");
}

void clean_exit(char* msg)
{
  printf(msg); 

  if (claimed)
    libusb_release_interface(handle, 1);

  if (detached)
    libusb_attach_kernel_driver(handle, 1);

  libusb_close(handle);
  libusb_exit(ctx);

  exit(0);
}
